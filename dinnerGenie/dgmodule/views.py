from django.http import HttpResponseRedirect
from django.shortcuts import render
from .forms import NameForm
# Create your views here.


def hello_world(request):
    return render(request, 'hello_world.html', {})


def render_test(request):
    return render(request, 'index.html', {})



# def genie_display(request):
#     # if this is a POST request we need to process the form data
#     if request.method == 'POST':
#         # create a form instance and populate it with data from the request:
#         form = NameForm(request.POST)
#         # check whether it's valid:
#         if form.is_valid():
#             # process the data in form.cleaned_data as required
#             # ...
#             # redirect to a new URL:
#             return HttpResponseRedirect('/')
#
#     # if a GET (or any other method) we'll create a blank form
#     else:
#         form = NameForm()
#
#     return render(request, 'index.html', {'form': form})



# def genie_display(request):
#     return render(request, 'index.html', {})

